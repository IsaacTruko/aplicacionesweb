import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './components/components.component';
import { LandingComponent } from './examples/landing/landing.component';
import { LoginComponent } from './examples/login/login.component';
import { ProfileComponent } from './examples/profile/profile.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { ModelosComponent } from './examples/modelos/modelos.component';
import { departamentoComponent } from './examples/departamento/departamento.component';
import { PuestoComponent } from './examples/puesto/puesto.component';
import { EmpleadoComponent } from './examples/empleado/empleado.component';


const routes: Routes =[
    { path: '', redirectTo: 'index', pathMatch: 'full' },
    { path: 'index',                 component: ComponentsComponent },
    { path: 'nucleoicons',           component: NucleoiconsComponent },
    { path: 'examples/landing',      component: LandingComponent },
    { path: 'examples/login',        component: LoginComponent },
    { path: 'examples/profile',      component: ProfileComponent },
    { path: 'examples/modelos',      component: ModelosComponent},
    { path: 'examples/departamento', component: departamentoComponent},
    { path: 'examples/puesto',       component: PuestoComponent},
    { path: 'examples/empleado',     component: EmpleadoComponent},
];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [
    ],
})
export class AppRoutingModule { }
