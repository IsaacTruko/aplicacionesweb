import { Component, OnInit } from '@angular/core';
import { GetApiService } from '../../get-api.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    data : Date = new Date();
    focus;
    focus1;
    input_user: string ="";
    input_password: string="";
    

    constructor(private api:GetApiService, private router: Router) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }Component
    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');

        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }

    login(){
        this.api.apiLoginEmp(this.input_user,this.input_password).subscribe(data => {
            if(data[0].status == '200'){
                console.info(data[0].datos[0].numero_empleado)
                this.router.navigate(['examples/modelos']);
            }

            else 
                console.warn("Usuario o contraseña incorrectos",data[0].status)
        }
         );
    }
}

