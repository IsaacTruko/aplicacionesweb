import { ComponentFixture, TestBed } from '@angular/core/testing';

import { departamentoComponent } from './departamento.component';

describe('departamentoComponent', () => {
  let component: departamentoComponent;
  let fixture: ComponentFixture<departamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ departamentoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(departamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
