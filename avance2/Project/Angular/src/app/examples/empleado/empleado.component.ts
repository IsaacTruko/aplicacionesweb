import { Component, OnInit } from '@angular/core';
import { GetApiService } from '../../get-api.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

export interface Items{
  idItem: number;
  nameItem: string;
}
export interface Departamento {
  id: number;
  nombre: string;
  descripcion: string;
}
export interface Puesto {
  id: string;
  nombre: string;
  descripcion: string;
}

export interface Permiso {
  id: string,
  nombre: string,
  c_usuario: boolean,
  v_usuario: boolean,
  e_usuario: boolean,
  c_orden: boolean,
  v_orden: boolean,
  e_orden: boolean,
  e_modelo: boolean,
}

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.scss']
})

export class EmpleadoComponent implements OnInit {

  closeResult: string;

  public alertsExitoso: Array<IAlert> = [];
  private backupExitoso: Array<IAlert>;

  public alertsError: Array<IAlert> = [];
  private backupError: Array<IAlert>;

  public DatosDepartamento: Array<Departamento> = [];
  public DatosEmpleado: Array<Empleado> = [];
  public DatosPuesto: Array<Puesto> = [];
  public DatosPermiso: Array<Permiso> = [];



  opcionSeleccionadoPermiso: number = 0;
  opcionSeleccionadoPuesto: string = '0';
  opcionSeleccionadoDepartamento: number = 0;
  

  displayedColumns: string[] = ['idItem', 'nameItem', 'options'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  itemList: Items[];

  input_id: number = 0;
  input_nombre: string = "";
  input_descripcion: string = "";
  input_apellidop: string = "";
  input_apellidom: string = "";
  input_password: string = "";
  input_permiso: number = 0;
  input_puesto: string = "";
  input_departamento: number = 0;

  public alertaExitoso = false;
  public alertaError = false;
  public datosDepartamento = true;

  btnGuardar = false;
  btnActualizar = false;
  btnDeshabilitar = false;

  tituloModal = false;
  tituloModalActualizar = false;
  tituloModalDeshabilitar = false;

  constructor(private api: GetApiService, private router: Router, private modalService: NgbModal) {
    this.alertsExitoso.push({
      id: 1,
      type: 'success',
      strong: 'Exitoso!',
      message: 'Registro exitoso.',
      icon: 'ui-2_like'
    });

    this.alertsError.push({
      id: 4,
      type: 'danger',
      strong: 'Error!',
      message: 'Registro no valido',
      icon: 'objects_support-17'
    });

    this.backupError = this.alertsError.map((alert: IAlert) => Object.assign({}, alert));
    this.backupExitoso = this.alertsExitoso.map((alert: IAlert) => Object.assign({}, alert));
  }

  public closeAlertExitoso(alert: IAlert) {
    const index: number = this.alertsExitoso.indexOf(alert);
    this.alertsExitoso.splice(index, 1);
  }

  public closeAlertError(alert: IAlert) {
    const index: number = this.alertsError.indexOf(alert);
    this.alertsError.splice(index, 1);
  }

  ngOnInit(): void {
    this.mostrarEmpleados();
    this.mostrarPermisos();
    this.mostrarDepartamentos();
    this.mostrarPuestos();
  }

  mostrarEmpleados() {
    this.api.apiConsultEmpleadoJoin().subscribe(data => {
      this.DatosEmpleado = []; 
      for (let index = 0; index < data[0].datos.length; index++) {
        this.DatosEmpleado.push({
          id: data[0].datos[index].id,
          nombrep: data[0].datos[index].nombre,
          apellidop: data[0].datos[index].apellidop,
          apellidom: data[0].datos[index].apellidom,
          password: data[0].datos[index].password,
          permiso: data[0].datos[index].nombre_permiso,
          id_permiso: data[0].datos[index].permiso,
          puesto: data[0].datos[index].nombre_puesto,
          id_puesto: data[0].datos[index].puesto,
          departamento: data[0].datos[index].nombre_departamento,
          id_departamento: data[0].datos[index].departamento
        });
        console.log(this.DatosEmpleado);
        
      }
    });
  }

  mostrarDepartamentos(){
    this.api.apiConsultDepartamento(0).subscribe(data => {
      
      for (let index = 0; index < data[0].datos.length; index++) {

        this.DatosDepartamento.push({
          id: data[0].datos[index].id,
          nombre: data[0].datos[index].nombre_departamento,
          descripcion: data[0].datos[index].descripcion
        });

        
      }
      this.DatosDepartamento = data[0].datos
    });
  }

  mostrarPermisos() {
    this.api.apiConsultPermiso().subscribe(data => {
      this.DatosPermiso = [];
      for (let index = 0; index < data[0].datos.length; index++) {
        this.DatosPermiso.push({
          id: data[0].datos[index].id,
          nombre: data[0].datos[index].nombre,
          c_usuario: data[0].datos[index].c_usuario,
          v_usuario: data[0].datos[index].v_usuario,
          e_usuario: data[0].datos[index].e_usuario,
          c_orden: data[0].datos[index].c_orden,
          v_orden: data[0].datos[index].v_orden,
          e_orden: data[0].datos[index].e_orden,
          e_modelo: data[0].datos[index].e_modelo,
        });
        // console.warn(this.DatosPermiso);
      }
          
    });
  }

  mostrarPuestos(){
    this.api.apiConsultPuesto().subscribe(data => {
      
      for (let index = 0; index < data[0].datos.length; index++) {

        this.DatosPuesto.push({
          id: data[0].datos[index].id,
          nombre: data[0].datos[index].nombre_puesto,
          descripcion: data[0].datos[index].descripcion
        });

        
      }
      this.DatosPuesto = data[0].datos
    });
  }


  openModal(content) {
    this.btnGuardar = true;
    this.btnActualizar = false;
    this.btnDeshabilitar=false;
    this.tituloModalDeshabilitar=false;
    this.tituloModal = true;
    this.tituloModalActualizar = false;
    this.alertaExitoso = false;
    this.alertaError = false;

    this.input_id = 0;
    this.input_nombre = "";
    this.input_descripcion = "";
    this.input_apellidop = "";
    this.input_apellidom = "";
    this.input_password = "";
    this.input_permiso = 0;
    this.input_puesto = "";
    this.input_departamento = 0;
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModalActualizar(content, id: number, nombrep: string, apellidop: string, apellidom: string, password: string,
    permiso: number, puesto: string, departamento: number) {
    this.btnGuardar = false;
    this.btnActualizar = true;
    this.tituloModal = false;
    this.tituloModalActualizar = true;
    this.alertaExitoso = false;
    this.alertaError = false;
    this.btnDeshabilitar=false;
    this.tituloModalDeshabilitar=false;

    this.input_id = id;
    this.input_nombre = nombrep;
    this.input_apellidop = apellidop;
    this.input_apellidom = apellidom;
    this.input_password = password;
    this.input_permiso = permiso;
    this.input_puesto = puesto;
    this.input_departamento = departamento;
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openModalDeshabilitar(content, id: number, nombre: string, apellidop: string, apellidom: string, password: string,
    permiso: number, puesto: string, departamento: number) {
    this.btnGuardar = false;
    this.btnActualizar = false;
    this.tituloModal = false;
    this.tituloModalActualizar = false;
    this.btnDeshabilitar = true;
    this.tituloModalDeshabilitar = true;
    this.alertaExitoso = false;
    this.alertaError = false;

    this.input_id = id;
    this.input_nombre = nombre;
    this.input_apellidop = apellidop;
    this.input_apellidom = apellidom;
    this.input_password = password;
    this.input_permiso = permiso;
    this.input_puesto = puesto;
    this.input_departamento = departamento;
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  guardarEmpleado() {
    console.log(this.input_nombre + "------" + this.input_descripcion)
    this.api.apiCrearEmpleado(this.input_nombre, this.input_apellidop,this.input_apellidom,this.input_password,this.opcionSeleccionadoPermiso,this.opcionSeleccionadoPuesto,this.opcionSeleccionadoDepartamento).subscribe(data => {
      if (data[0].status == '200') {
        this.alertaExitoso = true;
        this.input_nombre = '';
        this.input_apellidop = '';
        this.input_apellidom = '';
        this.input_password = '';
        this.input_permiso = 0;
        this.input_puesto = '';
        this.input_departamento = 0;
        this.alertaError = false;
        this.mostrarEmpleados();
      }

      else {
        console.warn("Error al crear Empleado", data[0].status)
        this.alertaError = true;
        this.alertaExitoso = false;
      }
    });
  }

  actualizarEmpleado() {
    console.log(this.input_id + "-------" + this.input_nombre + "------" + this.input_descripcion)
    this.api.apiActualizarEmpleado(this.input_id,this.input_nombre, this.input_apellidop,this.input_apellidom,this.input_password,this.opcionSeleccionadoPermiso,this.opcionSeleccionadoPuesto,this.opcionSeleccionadoDepartamento).subscribe(data => {
      if (data[0].status == '200') {
        this.alertaExitoso = true;
        this.alertaError = false;
        this.mostrarEmpleados();
      }

      else {
        console.warn("Error al crear Empleado", data[0].status)
        this.alertaError = true;
        this.alertaExitoso = false;
      }
    });
  }

  deshabilitarEmpleado() {
    this.api.apiDeshabilitarEmpleado(this.input_id).subscribe(data => {
      if (data[0].status == '200') {
        this.alertaExitoso = true;
        this.input_nombre = '';
        this.input_descripcion = '';
        this.alertaError = false;
        this.mostrarEmpleados();
      }

      else {
        console.warn("Error al crear departamento", data[0].status)
        this.alertaError = true;
        this.alertaExitoso = false;
      }
    });
  }

}

export interface IAlert {
  id: number;
  type: string;
  strong?: string;
  message: string;
  icon?: string;
}

export interface Empleado {
  id: number;
  nombrep: string;
  apellidop: string;
  apellidom: string;
  password: string;
  permiso: string;
  id_permiso:number;
  puesto: string;
  id_puesto:string;
  departamento: string;
  id_departamento:number;
}

