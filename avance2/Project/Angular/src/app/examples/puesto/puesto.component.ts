import { Component, OnInit } from '@angular/core';
import { GetApiService } from '../../get-api.service';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

export interface Items{
  idItem: number;
  nameItem: string;
}

@Component({
  selector: 'app-puesto',
  templateUrl: './puesto.component.html',
  styleUrls: ['./puesto.component.scss'],
})


export class PuestoComponent implements OnInit {

  closeResult: string;

  public alertsExitoso: Array<IAlert> = [];
  private backupExitoso: Array<IAlert>;

  public alertsError: Array<IAlert> = [];
  private backupError: Array<IAlert>;

  public DatosPuesto: Array<Puesto> = [];

  opcionSeleccionado: string  = '0';


  displayedColumns: string[] = ['idItem', 'nameItem', 'options'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  itemList: Items[];


  input_codigo: string ="";
  input_nombre: string ="";
  input_descripcion: string="";


  public alertaExitoso = false;
  public alertaError = false;
  public datosDepartamento = true;

  btnGuardar = false;
  btnActualizar = false;
  btnDeshabilitar = false;


  tituloModal = false;
  tituloModalActualizar = false;
  tituloModalDeshabilitar = false;




  constructor(private api:GetApiService, private router: Router,private modalService: NgbModal) { 

    this.alertsExitoso.push({
      id: 1,
      type: 'success',
      strong: 'Exitoso!',
      message: 'Registro exitoso.',
      icon: 'ui-2_like'
  });

  this.alertsError.push( {
    id: 4,
    type: 'danger',
    strong: 'Error!',
    message: 'Registro no valido',
    icon: 'objects_support-17'
});

  this.backupError = this.alertsError.map((alert: IAlert) => Object.assign({}, alert));
  this.backupExitoso = this.alertsExitoso.map((alert: IAlert) => Object.assign({}, alert));
  }


  public closeAlertExitoso(alert: IAlert) {
    const index: number = this.alertsExitoso.indexOf(alert);
    this.alertsExitoso.splice(index, 1);
}


public closeAlertError(alert: IAlert) {
  const index: number = this.alertsError.indexOf(alert);
  this.alertsError.splice(index, 1);
}


  ngOnInit(): void {

    this.mostrarPuestos();

  }


  mostrarPuestos(){
    this.api.apiConsultPuesto().subscribe(data => {
      
      for (let index = 0; index < data[0].datos.length; index++) {

        this.DatosPuesto.push({
          id: data[0].datos[index].id,
          nombre: data[0].datos[index].nombre_puesto,
          descripcion: data[0].datos[index].descripcion
        });

        
      }
      this.DatosPuesto = data[0].datos
    });
  }

  openModal(content){
    this.btnGuardar = true;
    this.btnActualizar = false;
    this.tituloModal = true;
    this.tituloModalActualizar= false;
    this.alertaExitoso = false;
    this.alertaError = false;

    this.input_codigo = '';
    this.input_nombre = '';
    this.input_descripcion = '';
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
  }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }

  openModalActualizar(content, id:string, nombre:string, descripcion:string){
    this.btnGuardar = false;
    this.btnActualizar = true;
    this.tituloModal = false;
    this.tituloModalActualizar= true;
    this.alertaExitoso = false;
    this.alertaError = false;

    this.input_codigo = id;
    this.input_nombre = nombre;
    this.input_descripcion = descripcion;
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
  }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }

  openModalDeshabilitar(content, id:string, nombre:string, descripcion:string){
    this.btnGuardar = false;
    this.btnActualizar = false;
    this.tituloModal = false;
    this.tituloModalActualizar= false;
    this.btnDeshabilitar = true;
    this.tituloModalDeshabilitar = true;
    this.alertaExitoso = false;
    this.alertaError = false;

    this.input_codigo = id;
    this.input_nombre = nombre;
    this.input_descripcion = descripcion;
    this.modalService.open(content, { windowClass: 'modal', size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
  }, (reason) => {

      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
  });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
    } else {
        return  `with: ${reason}`;
    }
}

guardarPuesto(){

  console.log(this.input_nombre +"------" +this.input_descripcion)
  this.api.apiCrearPuesto(this.input_codigo,this.input_nombre,this.input_descripcion).subscribe(data => {
    if(data[0].status == '200'){
      this.alertaExitoso = true;
      this.input_codigo = '';
      this.input_nombre = '';
      this.input_descripcion = '';
      this.alertaError = false;
      this.mostrarPuestos();
    }

    else {
      console.warn("Error al crear puestos",data[0].status)
      this.alertaError = true;
      this.alertaExitoso = false;
    }
});
}

actualizarPuesto(){

  console.log(this.input_codigo+"-------"+this.input_nombre +"------" +this.input_descripcion)
  this.api.apiActualizarPuesto(this.input_codigo,this.input_nombre,this.input_descripcion).subscribe(data => {
    if(data[0].status == '200'){
      this.alertaExitoso = true;
      this.alertaError = false;
      this.mostrarPuestos();
    }

    else {
      console.warn("Error al crear puesto",data[0].status)
      this.alertaError = true;
      this.alertaExitoso = false;
    }
});
}

deshabilitarPuesto(){
  
  this.api.apiDeshabilitarPuesto( this.input_codigo ).subscribe(data => {
    if(data[0].status == '200'){
      this.alertaExitoso = true;
      this.input_nombre = '';
      this.input_descripcion = '';
      this.alertaError = false;
      this.mostrarPuestos();
    }

    else {
      console.warn("Error al crear departamento",data[0].status)
      this.alertaError = true;
      this.alertaExitoso = false;
    }
});
}

}

export interface IAlert {
  id: number;
  type: string;
  strong?: string;
  message: string;
  icon?: string;
}

export interface Puesto {
  id: string;
  nombre: string;
  descripcion: string;
}
