const express = require('express');

const app = express();
const cors = require("cors");

app.use(express.json());
app.use(express.urlencoded({extended:true}))

var mysql = require('mysql');

var con = mysql.createConnection({
  host: "127.0.0.1",
  user: "root",
  password: "Master07?",
  database: "BYO_PRUEBAS"
});

app.use(cors());
app.get("/test", (req,res)=>{
res.send("server working");
});

app.listen(4002, () => {

  
  console.log('Listening on port 4002');
  con.connect(function(err) { console.log("DATABASE CONNECTED")});
});


// ----------------APIS EMPLEADO------------------------

// app.get('/api/consultEmpleados', (req, res) => {
//   console.log(req.query)
//   const sql_query = 'Select * from Empleado';
//  con.query(sql_query,(err, result) => {
//   res.send(result)
//  });
// });

app.post('/api/login', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]

  if((req.body.user && req.body.password ) != '' ){
  const sql_query = 'Select * from Empleado WHERE numero_empleado ='+req.body.user+' AND password ='+req.body.password;
 con.query(sql_query,(err, response) => {
  
    const extractedInfo = response.map(row => ({
      numero_empleado: row.numero_empleado
    }));

if (response.length > 0){
  responseJson[0].status = '200'
  responseJson[0].datos = extractedInfo
}
  res.json(responseJson)
 });
  }
  else 
  res.json(responseJson)
});

// app.get('/api/consultEmpleado', (req, res) => {
//   var responseJson = [{
//     "status": "404",
//     "datos" : []
//   }]


// const sql_query = `select * from Empleado WHERE activo = 1`;
// con.query(sql_query, (err, result) => {
//   if (err) {
//     responseJson.status = '500'
//     res.json(responseJson)

//   } else {

//     const extractedInfo = result.map(row => ({
//       id: row.numero_empleado,
//       nombre: row.nombre,
//       apellidop: row.apellido_paterno,
//       apellidom: row.apellido_materno,
//       password: row.password,
//       permiso: row.id_permiso,
//       puesto: row.id_puesto,
//       departamento: row.id_departamento,


//     }));


//     responseJson[0].status = '200'
//     responseJson[0].datos = extractedInfo

//     console.log(responseJson)
    
//     res.json(responseJson)
//   }
// });

  
// });

app.post('/api/crearEmpleado', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]
  console.log(req.body)
  if((req.body.nombre && req.body.apellidop && req.body.apellidom && req.body.password && req.body.permiso && req.body.puesto && req.body.departamento) != '' ){
console.log("Datos recibidos:", req.body);
const sql_query = `INSERT INTO Empleado (nombre, apellido_paterno, apellido_materno, password, id_permiso, id_puesto, id_departamento, activo) VALUES ('${req.body.nombre}','${req.body.apellidop}','${req.body.apellidom}','${req.body.password}','${req.body.permiso}','${req.body.puesto}','${req.body.departamento}', 1)`;
console.log("Query SQL:", sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});


app.put('/api/actualizarEmpleado', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]
  console.log(req.body)
  if((req.body.nombre && req.body.apellidop && req.body.apellidom && req.body.password && req.body.permiso && req.body.puesto && req.body.departamento && req.body.id) != '' ){

   const sql_query = 'UPDATE Empleado SET nombre ="'+ req.body.nombre +'", apellido_paterno ="'+ req.body.apellidop +'", apellido_materno ="'+ req.body.apellidom +'", password ="'+ req.body.password +'", id_permiso ='+ req.body.permiso +', id_puesto ="'+ req.body.puesto +'", id_departamento ='+ req.body.departamento +' WHERE numero_empleado ='+ req.body.id;

    console.log(req.body,sql_query);
    con.query(sql_query, (err, result) => {
      console.log(req.body)
      if (err) {
        console.log("Entro 1")
        responseJson.status = '500'
        res.json(responseJson)
    
      } else {
    
        console.log("Entro 2")
        responseJson[0].status = '200'
        res.json(responseJson)
      }
    });
    
      }
      else 
      {
        console.log("Entro al else")
    res.json(responseJson)}
    });
  

    app.put('/api/deshabilitarEmpleado', (req, res) => {
      var responseJson = [{
        "status": "404",
        "datos" : []
      }]
    
      if((req.body.id ) != '' ){
    
    const sql_query = 'UPDATE Empleado  SET activo = 0 WHERE numero_empleado ='+req.body.id
    console.log(req.body,sql_query);
    con.query(sql_query, (err, result) => {
      if (err) {
        console.log("Entro 1")
        responseJson.status = '500'
        res.json(responseJson)
    
      } else {
    
        console.log("Entro 2")
        responseJson[0].status = '200'
        res.json(responseJson)
      }
    });
    
      }
      else 
      {
        console.log("Entro al else")
    res.json(responseJson)}
    });

    app.get('/api/consultEmpleadoJoin', (req, res) => {
      const responseJson = [{
        "status": "404",
        "datos": []
      }];
    
      const sql_query = `
      SELECT E.numero_empleado,
      E.nombre,
      E.apellido_paterno,
      E.apellido_materno,
      E.password,
      E.id_permiso,
      E.id_puesto,
      E.id_departamento,
      D.nombre_departamento,
      Pe.nombre as nombre_permiso,
      Pu.nombre_puesto
      FROM Empleado as E
      INNER JOIN Departamento as D ON D.numero = E.id_departamento
      INNER JOIN Permiso as Pe ON Pe.numero = E.id_permiso
      INNER JOIN Puesto as Pu ON Pu.codigo_puesto = E.id_puesto
      WHERE E.activo = 1
      `;
      console.log(sql_query);
      con.query(sql_query, (err, result) => {
        if (err) {
          responseJson.status = '500';
          res.json(responseJson);
        } else {
          const extractedInfo = result.map(row => ({
            id: row.numero_empleado,
            nombre: row.nombre,
            apellidop: row.apellido_paterno,
            apellidom: row.apellido_materno,
            password: row.password,
            permiso: row.id_permiso,
            puesto: row.id_puesto,
            departamento: row.id_departamento,
            nombre_departamento: row.nombre_departamento,
            nombre_permiso: row.nombre_permiso,
            nombre_puesto: row.nombre_puesto,
          }));
    console.log(extractedInfo);
          responseJson[0].status = '200';
          responseJson[0].datos = extractedInfo;
    
          console.log(responseJson);
          res.json(responseJson);
        }
      });
    });

    



// ----------------APIS Cliente------------------------

// ----------------APIS Permiso------------------------

app.get('/api/consultPermiso', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]


const sql_query = `select * from Permiso`;
con.query(sql_query, (err, result) => {
  if (err) {
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    const extractedInfo = result.map(row => ({
      id: row.numero,
      nombre: row.nombre,
      c_usuario: row.c_usuario,
      v_usuario: row.v_usuario,
      e_usuario: row.e_usuario,
      c_orden: row.c_orden,
      v_orden: row.v_orden,
      e_orden: row.e_orden,
      e_modelo: row.e_modelo,


    }));
    console.log(extractedInfo);


    responseJson[0].status = '200'
    responseJson[0].datos = extractedInfo

    console.log(responseJson)
    
    res.json(responseJson)
  }
});

  
});

    
// ----------------APIS Departamento------------------------
    
    
    app.get('/api/consultDepartamento', (req, res) => {
      var responseJson = [{
        "status": "404",
        "datos" : []
      }]
      console.log(req.query)
      var sql_query = '';
      if (req.query.id != 0)
        sql_query = `select * from Departamento WHERE activo = 1 AND numero = `+req.query.id;
      else 
        sql_query = `select * from Departamento WHERE activo = 1`;

      console.log(sql_query)
    con.query(sql_query, (err, result) => {
      if (err) {
        responseJson.status = '500'
        res.json(responseJson)
    
      } else {
    
        const extractedInfo = result.map(row => ({
          id: row.numero,
          nombre: row.nombre_departamento,
          descripcion: row.descripcion
        }));
    
    
        responseJson[0].status = '200'
        responseJson[0].datos = extractedInfo
    
        console.log(responseJson)
        
        res.json(responseJson)
      }
    });
    
      
    });
    
app.post('/api/crearDepartamento', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]

  if((req.body.nombre && req.body.descripcion ) != '' ){

const sql_query = `INSERT INTO Departamento (nombre_departamento, descripcion, activo) VALUES ('${req.body.nombre}','${req.body.descripcion}', 1)`;
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});


app.put('/api/actualizarDepartamento', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]
  console.log(req.body)
  if((req.body.nombre && req.body.descripcion && req.body.id ) != '' ){

const sql_query = 'UPDATE Departamento  SET nombre_departamento ="'+ req.body.nombre +'", descripcion ="'+ req.body.descripcion+'" WHERE numero ='+req.body.id
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});

app.put('/api/deshabilitarDepartamento', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]

  if((req.body.id ) != '' ){

const sql_query = 'UPDATE Departamento  SET activo = 0 WHERE numero ='+req.body.id
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});




// ----------------APIS Puesto------------------------

app.get('/api/consultPuesto', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]

const sql_query = `select * from Puesto WHERE activo = 1`;
con.query(sql_query, (err, result) => {
  if (err) {
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    const extractedInfo = result.map(row => ({
      id: row.codigo_puesto,
      nombre: row.nombre_puesto,
      descripcion: row.descripcion
    }));


    responseJson[0].status = '200'
    responseJson[0].datos = extractedInfo

    console.log(responseJson)
    
    res.json(responseJson)
  }
});

  
});

app.post('/api/crearPuesto', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]

  if((req.body.id && req.body.nombre && req.body.descripcion ) != '' ){

const sql_query = `INSERT INTO Puesto (codigo_puesto, nombre_puesto, descripcion, activo) VALUES ('${req.body.id}','${req.body.nombre}','${req.body.descripcion}', 1 )`;
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});

app.put('/api/actualizarPuesto', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]
console.log(req.body)
  if((req.body.nombre && req.body.descripcion && req.body.id ) != '' ){

// const sql_query = 'UPDATE Puesto SET  nombre_puesto = "'+ req.body.nombre +'", descripcion ="'+ req.body.descripcion+'" WHERE codigo_puesto ='"+req.body.id";
const sql_query = 'UPDATE Puesto SET nombre_puesto = "' + req.body.nombre + '", descripcion = "' + req.body.descripcion + '" WHERE codigo_puesto = \'' + req.body.id + '\'';
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});

app.put('/api/deshabilitarPuesto', (req, res) => {
  var responseJson = [{
    "status": "404",
    "datos" : []
  }]
  console.log(req.body)
  
  if((req.body.id ) != '' ){

const sql_query = 'UPDATE Puesto  SET activo = 0 WHERE codigo_puesto = \'' + req.body.id + '\''
console.log(req.body,sql_query);
con.query(sql_query, (err, result) => {
  if (err) {
    console.log("Entro 1")
    responseJson.status = '500'
    res.json(responseJson)

  } else {

    console.log("Entro 2")
    responseJson[0].status = '200'
    res.json(responseJson)
  }
});

  }
  else 
  {
    console.log("Entro al else")
res.json(responseJson)}
});





// ----------------APIS Modelo ------------------------


app.get('/consultModelos', (req, res) => {
  console.log(req.query)
  const sql_query = 'Select * from Modelo';
 con.query(sql_query,(err, result) => {
  res.send(result)
 });
});

app.post('/createModelo', (req, res) => {
  console.log(req.body);
  const sql_query = `INSERT INTO Modelo (nombre, ancho, largo, altura, id_plywood, id_lumber, id_cliente, id_estatus) VALUES ('${req.body.nombre}','${req.body.ancho}','${req.body.largo}','${req.body.altura}','${req.body.id_plywood}','${req.body.id_lumber}','${req.body.id_cliente}','${req.body.id_estatus}')`;
  console.log(req.body);
  con.query(sql_query, (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error al crear el modelo');
    } else {
      res.send(result);
    }
  });
});

app.post('/updateModelo', (req, res) => {
  console.log(req.body);
  const sql_query = `UPDATE Modelo (nombre, ancho, largo, altura, id_plywood, id_lumber, id_cliente, id_estatus) VALUES ('${req.body.nombre}','${req.body.ancho}','${req.body.largo}','${req.body.altura}','${req.body.id_plywood}','${req.body.id_lumber}','${req.body.id_cliente}','${req.body.id_estatus}')`;
  console.log(req.body);
  con.query(sql_query, (err, result) => {
    if (err) {
      console.error(err);
      res.status(500).send('Error al crear el modelo');
    } else {
      res.send(result);
    }
  });
});











