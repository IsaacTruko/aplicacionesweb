-- MySQL dump 10.13  Distrib 8.0.35, for Linux (x86_64)
--
-- Host: localhost    Database: BYO_PRUEBAS
-- ------------------------------------------------------
-- Server version	8.0.35-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Cliente` (
  `numero_cliente` int NOT NULL AUTO_INCREMENT COMMENT 'Número de Cliente',
  `nombre_empresa` varchar(100) NOT NULL COMMENT 'Nombre de la Empresa',
  `rfc_empresa` varchar(15) NOT NULL COMMENT 'RFC de la Empresa',
  `correo` varchar(100) NOT NULL COMMENT 'Correo Electrónico',
  `password` varchar(255) NOT NULL COMMENT 'Contraseña',
  PRIMARY KEY (`numero_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Cliente`
--

LOCK TABLES `Cliente` WRITE;
/*!40000 ALTER TABLE `Cliente` DISABLE KEYS */;
INSERT INTO `Cliente` VALUES (1,'Empresa ABC S.A. de C.V.','ABC123456XYZ','contacto@empresaabc.com','contrasena123'),(2,'Compañía XYZ, S.A.','XYZ789456ABC','info@companiaxyz.com','claveXYZ2023'),(3,'Productos Innovadores, Inc.','PINNOV123456','ventas@productosinnovadores.com','securepass2023');
/*!40000 ALTER TABLE `Cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Departamento`
--

DROP TABLE IF EXISTS `Departamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Departamento` (
  `numero` int NOT NULL AUTO_INCREMENT COMMENT 'Número del Departamento',
  `nombre_departamento` varchar(100) NOT NULL COMMENT 'Nombre del Departamento',
  `descripcion` varchar(255) NOT NULL COMMENT 'Descripcion del departamento',
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Departamento`
--

LOCK TABLES `Departamento` WRITE;
/*!40000 ALTER TABLE `Departamento` DISABLE KEYS */;
INSERT INTO `Departamento` VALUES (1,'Calidad','si',1),(2,'Alexis','undefined',0),(3,'Producción','Encargado de la línea de producción de productos.',1),(17,'34','35',0),(18,'klajdf','s,mdfs',0),(19,'fghcg','dfggc',0),(20,'huih','bnjink',0),(21,'hftf','tyyfthfgv',0),(22,'Inspecto','Realiza inspección y control de calidad.',0),(23,'Inspector','Realiza inspección y control de calidad.',0),(24,'Gerencia','Encargado de la gestión y supervisión del personal.',1),(25,'huih','holaa',0),(26,'ffe','fefsd',0),(27,'fygyu','22222222222222',0),(28,'123445','22222222',0),(29,'222','22346',0),(30,'hjvjhgb','nhkbhvhjftyruyf',0),(31,'Si','Si',0);
/*!40000 ALTER TABLE `Departamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Empleado`
--

DROP TABLE IF EXISTS `Empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Empleado` (
  `numero_empleado` int NOT NULL AUTO_INCREMENT COMMENT 'Número de Empleado',
  `nombre` varchar(50) NOT NULL COMMENT 'Nombre',
  `apellido_paterno` varchar(50) NOT NULL COMMENT 'Apellido Paterno',
  `apellido_materno` varchar(50) DEFAULT NULL COMMENT 'Apellido Materno',
  `password` varchar(255) NOT NULL COMMENT 'Contraseña',
  `id_permiso` int NOT NULL COMMENT 'ID del Permiso',
  `id_puesto` varchar(10) NOT NULL COMMENT 'Código del Puesto',
  `id_departamento` int NOT NULL COMMENT 'ID del Departamento',
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`numero_empleado`),
  KEY `id_permiso` (`id_permiso`),
  KEY `id_puesto` (`id_puesto`),
  KEY `id_departamento` (`id_departamento`),
  CONSTRAINT `Empleado_ibfk_1` FOREIGN KEY (`id_permiso`) REFERENCES `Permiso` (`numero`),
  CONSTRAINT `Empleado_ibfk_2` FOREIGN KEY (`id_puesto`) REFERENCES `Puesto` (`codigo_puesto`),
  CONSTRAINT `Empleado_ibfk_3` FOREIGN KEY (`id_departamento`) REFERENCES `Departamento` (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Empleado`
--

LOCK TABLES `Empleado` WRITE;
/*!40000 ALTER TABLE `Empleado` DISABLE KEYS */;
INSERT INTO `Empleado` VALUES (1,'Isaac','Gonzalez','Gutierrez','12345',1,'GER',1,1),(2,'Alexis','Mendoza','Diaz','54321',2,'SUP',1,1),(3,'Jesus','Ruiz','Ruiz','12345',2,'GER',2,0),(4,'NuevoNombre','NuevoApellidoPaterno','NuevoApellidoMaterno','NuevaPassword',1,'GER',3,0),(5,'Jesus','Ruiz','Lopez','12345',2,'GER',2,0),(6,'1111111111111111','111111111111111','11111111111111111111','111111111111111111',1,'GER',1,0),(7,'Jesus','Ruiz','Lopez','12345',2,'GER',2,1),(8,'Isaac','Gonzalez','Gutierrez','12345',1,'GER',1,0),(9,'Isaac','Gonzalez Gutierrez','.','111111',1,'GER',1,0);
/*!40000 ALTER TABLE `Empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Estatus`
--

DROP TABLE IF EXISTS `Estatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Estatus` (
  `codigo_estatus` varchar(10) NOT NULL COMMENT 'Código del Estatus',
  `nombre_estatus` varchar(100) NOT NULL COMMENT 'Nombre del Estatus',
  `tipo_estatus` varchar(50) NOT NULL COMMENT 'Tipo de Estatus',
  PRIMARY KEY (`codigo_estatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Estatus`
--

LOCK TABLES `Estatus` WRITE;
/*!40000 ALTER TABLE `Estatus` DISABLE KEYS */;
INSERT INTO `Estatus` VALUES ('ACEPT','Aceptado','Modelo'),('ANUL','Anulada','Factura'),('CANCEL','Cancelada','Orden'),('COMPL','Completada','Orden'),('CREA','Creada','Orden'),('ENPROC','En Proceso','Orden'),('FACT','Facturada','Factura'),('PAG','Pagada','Factura'),('RECH','Rechazado','Modelo'),('REV','En Revisión','Modelo');
/*!40000 ALTER TABLE `Estatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Factura`
--

DROP TABLE IF EXISTS `Factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Factura` (
  `numero_factura` int NOT NULL AUTO_INCREMENT COMMENT 'Número de Factura',
  `fecha` date NOT NULL COMMENT 'Fecha de la Factura',
  `subtotal` decimal(10,2) NOT NULL COMMENT 'Subtotal',
  `gastos_operaciones` decimal(10,2) NOT NULL COMMENT 'Gastos de Operaciones',
  `iva` decimal(10,2) NOT NULL COMMENT 'IVA',
  `total` decimal(10,2) NOT NULL COMMENT 'Total',
  `total_despues_impuestos` decimal(10,2) NOT NULL COMMENT 'Total Después de Impuestos',
  `id_orden` int NOT NULL COMMENT 'ID de la Orden',
  `id_estatus` varchar(10) NOT NULL COMMENT 'ID del Estatus',
  `id_cliente` int NOT NULL COMMENT 'ID del Cliente',
  PRIMARY KEY (`numero_factura`),
  KEY `id_orden` (`id_orden`),
  KEY `id_estatus` (`id_estatus`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `Factura_ibfk_1` FOREIGN KEY (`id_orden`) REFERENCES `Orden` (`numero_orden`),
  CONSTRAINT `Factura_ibfk_2` FOREIGN KEY (`id_estatus`) REFERENCES `Estatus` (`codigo_estatus`),
  CONSTRAINT `Factura_ibfk_3` FOREIGN KEY (`id_cliente`) REFERENCES `Cliente` (`numero_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Factura`
--

LOCK TABLES `Factura` WRITE;
/*!40000 ALTER TABLE `Factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `Factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Historial`
--

DROP TABLE IF EXISTS `Historial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Historial` (
  `id_historial` int NOT NULL AUTO_INCREMENT COMMENT 'ID del Historial',
  `fecha` date NOT NULL COMMENT 'Fecha del Evento',
  `comentario` text NOT NULL COMMENT 'Comentario del Evento',
  `id_empleado` int NOT NULL COMMENT 'ID del Empleado',
  `id_orden` int NOT NULL COMMENT 'ID de la Orden',
  PRIMARY KEY (`id_historial`),
  KEY `id_empleado` (`id_empleado`),
  KEY `id_orden` (`id_orden`),
  CONSTRAINT `Historial_ibfk_1` FOREIGN KEY (`id_empleado`) REFERENCES `Empleado` (`numero_empleado`),
  CONSTRAINT `Historial_ibfk_2` FOREIGN KEY (`id_orden`) REFERENCES `Orden` (`numero_orden`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Historial`
--

LOCK TABLES `Historial` WRITE;
/*!40000 ALTER TABLE `Historial` DISABLE KEYS */;
INSERT INTO `Historial` VALUES (3,'2023-10-26','Se inicia la orden',1,4),(4,'2023-10-26','Se inicia la orden',2,5);
/*!40000 ALTER TABLE `Historial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lumber`
--

DROP TABLE IF EXISTS `Lumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Lumber` (
  `codigo_lumber` int NOT NULL COMMENT 'Código del Lumber',
  `nombre_lumber` varchar(100) NOT NULL COMMENT 'Nombre del Lumber',
  `grosor` decimal(5,2) NOT NULL COMMENT 'Grosor (en mm)',
  `ancho` decimal(5,2) NOT NULL COMMENT 'Ancho (en mm)',
  `costo_por_centimetro` decimal(10,2) NOT NULL COMMENT 'Costo por Centímetro Cuadrado',
  PRIMARY KEY (`codigo_lumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lumber`
--

LOCK TABLES `Lumber` WRITE;
/*!40000 ALTER TABLE `Lumber` DISABLE KEYS */;
INSERT INTO `Lumber` VALUES (1,'Pino Dimensionado',2.54,15.24,0.00),(2,'Roble Dimensionado',3.81,20.32,0.00),(3,'Cedro Dimensionado',2.22,10.16,0.00);
/*!40000 ALTER TABLE `Lumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Modelo`
--

DROP TABLE IF EXISTS `Modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Modelo` (
  `numero_modelo` int NOT NULL AUTO_INCREMENT COMMENT 'Número de Modelo',
  `nombre` varchar(255) DEFAULT NULL,
  `ancho` decimal(10,2) NOT NULL COMMENT 'Ancho (en cm)',
  `largo` decimal(10,2) NOT NULL COMMENT 'Largo (en cm)',
  `altura` decimal(10,2) NOT NULL COMMENT 'Altura (en cm)',
  `id_plywood` int NOT NULL COMMENT 'ID del Plywood',
  `id_lumber` int NOT NULL COMMENT 'ID del Lumber',
  `id_cliente` int NOT NULL COMMENT 'ID del Cliente',
  `id_estatus` varchar(10) NOT NULL COMMENT 'ID del Estatus',
  PRIMARY KEY (`numero_modelo`),
  KEY `id_plywood` (`id_plywood`),
  KEY `id_lumber` (`id_lumber`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_estatus` (`id_estatus`),
  CONSTRAINT `Modelo_ibfk_1` FOREIGN KEY (`id_plywood`) REFERENCES `Plywood` (`codigo`),
  CONSTRAINT `Modelo_ibfk_2` FOREIGN KEY (`id_lumber`) REFERENCES `Lumber` (`codigo_lumber`),
  CONSTRAINT `Modelo_ibfk_3` FOREIGN KEY (`id_cliente`) REFERENCES `Cliente` (`numero_cliente`),
  CONSTRAINT `Modelo_ibfk_4` FOREIGN KEY (`id_estatus`) REFERENCES `Estatus` (`codigo_estatus`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Modelo`
--

LOCK TABLES `Modelo` WRITE;
/*!40000 ALTER TABLE `Modelo` DISABLE KEYS */;
INSERT INTO `Modelo` VALUES (1,'Modelo1',40.50,50.70,30.24,10001,1,1,'REV'),(2,'Modelo2',30.50,20.70,70.24,10002,2,2,'ACEPT'),(3,'HOLA',10.20,20.30,5.50,10001,2,3,'REV'),(4,'500-32456',10.20,20.30,5.50,10001,2,2,'REV');
/*!40000 ALTER TABLE `Modelo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orden`
--

DROP TABLE IF EXISTS `Orden`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Orden` (
  `numero_orden` int NOT NULL AUTO_INCREMENT COMMENT 'Número de Orden',
  `cantidad_cajas` int NOT NULL COMMENT 'Cantidad de Cajas',
  `id_modelo` int NOT NULL COMMENT 'ID del Modelo',
  `id_cliente` int NOT NULL COMMENT 'ID del Cliente',
  `id_estatus` varchar(10) NOT NULL COMMENT 'ID del Estatus',
  PRIMARY KEY (`numero_orden`),
  KEY `id_modelo` (`id_modelo`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_estatus` (`id_estatus`),
  CONSTRAINT `Orden_ibfk_1` FOREIGN KEY (`id_modelo`) REFERENCES `Modelo` (`numero_modelo`),
  CONSTRAINT `Orden_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `Cliente` (`numero_cliente`),
  CONSTRAINT `Orden_ibfk_3` FOREIGN KEY (`id_estatus`) REFERENCES `Estatus` (`codigo_estatus`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orden`
--

LOCK TABLES `Orden` WRITE;
/*!40000 ALTER TABLE `Orden` DISABLE KEYS */;
INSERT INTO `Orden` VALUES (4,10,1,1,'CREA'),(5,20,2,3,'ENPROC');
/*!40000 ALTER TABLE `Orden` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Permiso`
--

DROP TABLE IF EXISTS `Permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Permiso` (
  `numero` int NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `nombre` varchar(255) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `c_usuario` tinyint(1) DEFAULT NULL COMMENT 'Create User Permission',
  `v_usuario` tinyint(1) DEFAULT NULL COMMENT 'View User Permission',
  `e_usuario` tinyint(1) DEFAULT NULL COMMENT 'Edit User Permission',
  `c_orden` tinyint(1) DEFAULT NULL COMMENT 'Create Order Permission',
  `v_orden` tinyint(1) DEFAULT NULL COMMENT 'View Order Permission',
  `e_orden` tinyint(1) DEFAULT NULL COMMENT 'Edit Order Permission',
  `e_modelo` tinyint(1) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Permiso`
--

LOCK TABLES `Permiso` WRITE;
/*!40000 ALTER TABLE `Permiso` DISABLE KEYS */;
INSERT INTO `Permiso` VALUES (1,'Admin','2023-10-26 16:59:56',1,1,1,1,1,1,1,1),(2,'editar_usuario','2023-10-26 17:01:00',1,1,1,0,0,0,0,1),(3,'editar_orden','2023-10-26 17:04:52',0,0,0,1,1,1,0,1),(4,'editar_modelo','2023-10-26 17:04:54',0,0,0,0,0,0,1,1);
/*!40000 ALTER TABLE `Permiso` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_unicode_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'IGNORE_SPACE,ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `before_permiso_insert` BEFORE INSERT ON `Permiso` FOR EACH ROW BEGIN
    SET NEW.create_time = NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Plywood`
--

DROP TABLE IF EXISTS `Plywood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Plywood` (
  `codigo` int NOT NULL COMMENT 'Código del Plywood',
  `nombre_plywood` varchar(100) NOT NULL COMMENT 'Nombre del Plywood',
  `grosor` decimal(5,2) NOT NULL COMMENT 'Grosor (en mm)',
  `ancho` decimal(5,2) NOT NULL COMMENT 'Ancho (en mm)',
  `costo_por_centimetro_cuadrado` decimal(10,2) NOT NULL COMMENT 'Costo por Centímetro Cuadrado',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Plywood`
--

LOCK TABLES `Plywood` WRITE;
/*!40000 ALTER TABLE `Plywood` DISABLE KEYS */;
INSERT INTO `Plywood` VALUES (10001,'Plywood ACX Exterior',1.27,121.90,250.00),(10002,'Plywood CDX Exterior',1.59,122.00,280.00),(10003,'Plywood de Pino BCX',0.95,120.00,240.00);
/*!40000 ALTER TABLE `Plywood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Puesto`
--

DROP TABLE IF EXISTS `Puesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Puesto` (
  `codigo_puesto` varchar(10) NOT NULL COMMENT 'Código del Puesto',
  `nombre_puesto` varchar(100) NOT NULL COMMENT 'Nombre del Puesto',
  `descripcion` varchar(255) NOT NULL COMMENT 'Nombre del Puesto',
  `activo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`codigo_puesto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Puesto`
--

LOCK TABLES `Puesto` WRITE;
/*!40000 ALTER TABLE `Puesto` DISABLE KEYS */;
INSERT INTO `Puesto` VALUES ('11111111','Gerente5','Encargado de la gestión y supervisActualizarión del personal.',0),('fyugyjh','hgjgjy','igiyuguih',0),('GER','Gerente','Encargado de la gestión y supervisión del personal.',1),('GER2','Gerente','Encargado de la gestión y supervisión del personal.',0),('GER3','Gerente3','Encargado de la gestión y supervisión del personal.',1),('hola','hola','hola',0),('INS','Inspector','Realiza inspección y control de calidad.',1),('PROD','Producción','Trabaja en la línea de producción de productos.',1),('SUP','Supervisor','Supervisa y coordina el trabajo de un equipo.',1),('undefined','hjcbjahsb','feqin',0),('vbhjbjh','ygybj','ehwehfkejrnfjme',0),('ygyuguy','hfgghfgh','hhfhgf',0);
/*!40000 ALTER TABLE `Puesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Solicitud_Modelo`
--

DROP TABLE IF EXISTS `Solicitud_Modelo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Solicitud_Modelo` (
  `id_solicitud` int NOT NULL AUTO_INCREMENT COMMENT 'ID de la Solicitud',
  `fecha` date NOT NULL COMMENT 'Fecha de la Solicitud',
  `comentario` text COMMENT 'Comentario (en caso de rechazo)',
  `id_empleado` int NOT NULL COMMENT 'ID del Empleado',
  `id_modelo` int NOT NULL COMMENT 'ID del Modelo',
  PRIMARY KEY (`id_solicitud`),
  KEY `id_empleado` (`id_empleado`),
  KEY `id_modelo` (`id_modelo`),
  CONSTRAINT `Solicitud_Modelo_ibfk_1` FOREIGN KEY (`id_empleado`) REFERENCES `Empleado` (`numero_empleado`),
  CONSTRAINT `Solicitud_Modelo_ibfk_2` FOREIGN KEY (`id_modelo`) REFERENCES `Modelo` (`numero_modelo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Solicitud_Modelo`
--

LOCK TABLES `Solicitud_Modelo` WRITE;
/*!40000 ALTER TABLE `Solicitud_Modelo` DISABLE KEYS */;
/*!40000 ALTER TABLE `Solicitud_Modelo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-11-07  9:08:44
