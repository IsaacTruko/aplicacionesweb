from django.urls import path




from home import views


app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('logout/', views.logoutView, name="logout"),
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('detail/profile/<int:pk>/', views.ProfileView.as_view(), name="profile_view"),
    path('udpate/profile/<int:pk>/', views.UpdateProfile.as_view(), name="update_profile"),
]