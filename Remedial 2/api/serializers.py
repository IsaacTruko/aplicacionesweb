



from rest_framework import serializers

from django.contrib.auth.models import User
from core.models import GrantGoal



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "username",
            "first_name",
            "last_name",
            "email"
        ]


class GrantGoalSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = GrantGoal
        fields = "__all__"

class GrantGoalSerializerDetail(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"

class GrantGoalSerializerCreate(serializers.ModelSerializer):
    class Meta:
        model = GrantGoal
        fields = "__all__"