from django.shortcuts import render

from django.views import generic
from django.urls import reverse_lazy

from .models import GrantGoal
from .forms import GrantGoalForm, UpdateGrantGoalForm

# Create your views here.


# # # #   G R A N T G O A L  C R U D  # # # # # 

## CREATE GRANT GOAL
class CreateGrantGoal(generic.CreateView):
    template_name = "core/create.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy('core:list_grantgoal')

## RETRIEVE GRANT GOAL
# LIST GRANT GOAL
class ListGrantGoal(generic.ListView):
    template_name = "core/list.html"
    model = GrantGoal #queryset = GrantGoal.objects.filter(status=True)

# DETAIL GRANT GOAL
class DetailGrantGoal(generic.View):
    template_name = "core/detail.html"
    context = {}

    def get(self, request, pk):
        grantgoal = GrantGoal.objects.get(id=pk)
        self.context = {
            "grantgoal": grantgoal
        }
        return render(request, self.template_name, self.context)


## UPDATE GRANT GOAL
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/update.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy('core:list_grantgoal')

## DELETE GRANT GOAL
class DeleteGrantGoal(generic.DeleteView):
    template_name = "core/delete.html"
    model = GrantGoal
    success_url = reverse_lazy("core:list_grantgoal")